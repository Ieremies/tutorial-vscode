# Instalação

## Instalação Windows
Acesse [o site oficial](https://code.visualstudio.com/) e faça o download indicado para Windows (versão estável):

![image](./images/image1.png)

Salve o arquivo e, quando finalizado o download, abra-o para fazer a instalação. Siga os passos indicados no instalador. Ao finalizar, você deve ter seu VSCode instalado e funcionando.

## Instalação Ubuntu

Para instalar o VSCode no Ubuntu você deve ir na loja de aplicativos do Ubuntu e pesquisar por *visual studio code*. Lá você conseguirá fazer toda a instalação.

![image](./images/image8.png)

Caso prefira fazer pela linha de comando, use `sudo snap install --classic code`.

# Configuração e primeiros passos

## Terminal

Para abrir um terminal, use `Ctrl + Shift + ‘` . Se você estiver no Windows, por padrão, o terminal aberto usará a shell do windows, powershell. Recomendamos fortemente trocar esse padrão para uma shell do Linux, como Bash.

![image](./images/image6.png)

Se você já instalou Git no seu sistema, mudar é mais fácil.
- Abra um novo terminal,
- no canto direito dele haverá um drop down menu escrito *1: powershell*, como ilustrado na figura.
- Clique nele, selecione a opção *Select Default Shell* e
- selecione a opção *Git Bash*.

Agora você pode fechar esse terminal e abrir outro que ele já estará com a Bash.

![image](./images/image2.png)

## Inicializar repositório da disciplina

Durante essa disciplina usaremos o sistema de versionamento Git e o server do IC no GitLab para submeter e postar novas tarefas.

Começaremos criando uma pasta, digamos *MC202*, onde colocaremos os arquivos da disciplina. No canto superior esquerdo, no menu *Files*, clique em abrir uma nova pasta (ou com o atalho `Ctrl+K Ctrl+O`). Selecione a pasta na qual deseja guardar seus arquivos da disciplina.

Após adicionada a pasta ao seu VSCode, podemos agora clonar o seu repositório no GitLab. Para abrir a paleta de comandos use `Ctrl + Shift + P` , digite *git clone*, selecione esse comando e, no campo onde você digitou o comando, digite o url do seu repositório. Lembre-se, seu url é `https://gitlab.ic.unicamp.br/mc202-2020/ra123456` , trocando `123456` pelo seu RA. Digite o url e aperte enter. Selecione a pasta que criamos para a disciplina. Após, deverá aparecer um pop-up do Windows perguntando seu usuário e senha do Gitlab do IC, o Windows guardará-os para você. Preencha-os e aperte enter.

Caso você troque sua senha ou tenha escrito errado, tudo o que precisa fazer é
- pesquisar no menu Iniciar do Windows por *credenciais*,
- selecionar o *Gerenciador de Credenciais*,
- ir em *Credenciais do Windows* e
- editar a entrada correspondente a `git:https://gitlab.ic.unicamp.br` com a senha correta.

Refazendo os passos descritos anteriormente você deve ser capaz de clonar seu repositório.

![image](./images/image7.png)

Pronto, agora você deverá ter uma pasta chamada `ra123456` (seu RA) com as suas tarefas, como mostrado na figura. Lembre-se que para começar a usar o git, você deve cadastrar seu nome e seu email, com `git config --global user.name SEU_NOME` e `git config --global user.email SEU_EMAIL`.

## Plugins úteis

Algumas extensões do VSCode serão extremamentes úteis para nossa disciplina. Elas farão com que o nosso editor contenha tudo que precisamos. Para instalar essas extensões, o processo é o mesmo.

No menu lateral à esquerda, a última opção da parte superior, é onde encontramos nossas extensões. Você clicar diretamente no ícone ou usar o atalho `Crtl + shift + x` para acessá-las.

![image](./images/image3.png)

Na barra de pesquisa no segundo painel, digite o nome do plugin que deseja instalar, selecione-o dentre os resultados que surgirem e clique no botão verde Install. Para que o plugin tenha efeito, você deve reiniciar o VSCode.

![image](./images/image4.png)

Aqui vai uma lista de plugins que nós recomendamos:
- C/C++ - suporte para C
- Visual Studio Intellicode - autocomplete
- Bracket Pair Colorizer - identifica as duplas de chaves

## Workflow do Git

![image](./images/image5.png)

Por fim, gostaríamos de demonstrar algumas funcionalidades que você pode utilizar.

O terceiro ícone do menu lateral esquerdo permite você acessar seu controle e versões (`Ctrl + shift + g`). Nele ele irá mostrar quais arquivos diferem do último commit e, clicando nele, as diferenças.

Na seleção do arquivo, no painel escrito *Changes*, você pode, da esquerda para a direita, abrir o arquivo, reverter as mudanças ou dar stage. Ao adicionar os arquivos desejados ao stage, no campo *Message* você pode escrever sua mensagem de commit (boas mensagens, descritivas facilitam muito a sua vida) e com `Ctrl + enter` você pode dar o commit.

Lembre-se que até então suas mudanças são apenas locais e não no GitLab do IC. Para sincronizar com seu remoto, utilize o segundo botão da esquerda do menu azul no canto inferior.

Além disso, todos esse comandos podem ser utilizados manualmente no terminal que configuramos anteriormente.
